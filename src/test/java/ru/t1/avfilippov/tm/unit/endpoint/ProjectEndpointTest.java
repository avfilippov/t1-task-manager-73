package ru.t1.avfilippov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Transactional
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectEndpointTest {

    private static final String PROJECT_URL = "http://localhost:8080/api/projects";

    private static final String PROJECTS_URL = "http://localhost:8080/api/projects";

    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        final Authentication authentication;
        authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        add(project1);
        add(project2);
    }

    @After
    @SneakyThrows
    public void afterTest() {
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL + "/clear")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(final ProjectDto projectDto) {
        String url = PROJECT_URL + "/save";
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        System.out.println(json);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void addAll(final List<ProjectDto> projects) {
        String url = PROJECTS_URL + "/add";
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private ProjectDto findById(final String id) {
        String url = PROJECT_URL + "/findById/" + id;
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDto.class);
    }

    @SneakyThrows
    private List<ProjectDto> findAll() {
        String url = PROJECTS_URL + "/findAll";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDto[].class));
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        add(project3);
        final ProjectDto project = findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.getId(), project.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(findById(project1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeTest() {
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL + "/clear")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testSave() {
        add(project4);
        final ProjectDto project = findById(project4.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project4.getId(), project.getId());
    }

}
