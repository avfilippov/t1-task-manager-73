package ru.t1.avfilippov.tm.unit.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.repository.TaskDtoRepository;
import ru.t1.avfilippov.tm.util.UserUtil;

import javax.transaction.Transactional;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskDtoRepositoryTest {

    private final TaskDto task1 = new TaskDto("Test Task 1");

    private final TaskDto task2 = new TaskDto("Test Task 2");

    private final TaskDto task3 = new TaskDto("Test Task 3");

    private final TaskDto task4 = new TaskDto("Test Task 4");

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TaskDtoRepository taskRepository;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);
    }

    @Autowired
    public void clean() {
        taskRepository.deleteAll();
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByUserIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task2.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task2.getId()).orElse(null));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existByUserIdAndIdTest() {
        Assert.assertTrue(taskRepository.existsById(task3.getId()));
    }

}
