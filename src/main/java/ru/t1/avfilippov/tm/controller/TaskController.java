package ru.t1.avfilippov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.CustomUser;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.enumerated.Status;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ProjectDTOService projectService;

    @Autowired
    private TaskDTOService taskService;

    @GetMapping("/task/create")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        System.out.println("User in task controller " + user.getUserId());
        taskService.save(user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDto task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final TaskDto task = taskService.findByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("taskId", task.getId());
        modelAndView.addObject("projects", getProjects(user));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

    private Collection<ProjectDto> getProjects(
            @AuthenticationPrincipal final CustomUser user
    ) {
        return projectService.findAll(user.getUserId());
    }

}
